// Interfaz que implementan todos las clases que son campos en el juego
public interface CampodeJuego {
    String getIcono();
    Constante.Reporte recibirDisparo();
}