public class Barco {
    private final String nombre;
    private final Constante.Barco tipo;
    private final int tamaño;
    private int salud;

    // Constructor del barco
    public Barco(Constante.Barco tipo, String nombre, int tamaño){
        this.tipo = tipo;
        this.nombre = nombre;
        this.tamaño = tamaño;
        this.salud = tamaño;
    }
        // Retorna el estado de salud del barco
    public Constante.Reporte reporteDeDaños() {
        if(salud == 0) {
            return Constante.Reporte.Hundido;
        } else if(salud < tamaño) {
            return Constante.Reporte.Dañado;
        } else {
            return Constante.Reporte.SinDaños;
        }
    }

    // Le baja la salud al barco e indica si este fue destruido
    public void recibirDaño() {

        if(salud > 0) {

            System.out.printf("Disparo acertado. El %s: %s  ha sido dañado\n ", tipo, nombre);
            salud--;
        } else {
            System.out.printf("El %s: %s fue destruido \n", tipo, nombre);
        }
    }
    // get del atributo tipo
    public Constante.Barco getTipo() {
        return tipo;
    }

    // get del atributo nombre
    public String getNombre() {
        return nombre;
    }
    // get del atributo tamaño
    public int getTamaño() {
        return tamaño;
    }

}

