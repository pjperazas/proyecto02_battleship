public class Constante {
// Colores

    public static final String Verde = "\u001B[32m";
    public static final String Azul = "\u001B[34m";
    public static final String Blanco = "\u001B[37m";
    public static final String Rojo = "\u001B[31m";
    public static final String Negro = "\u001B[30m";
    public static final String reset = "\u001B[0m";




    // Iconos Del barco
    public static final String PARTE_EN_LLAMAS =  Constante.Rojo+ "♨" + Constante.reset;
    public static final String BARCO_FULLVIDA = Constante.Verde + "□" + Constante.reset;
    public static final String BARCO_DAÑADO = Constante.Rojo + "□" + Constante.reset;
    public static final String BARCO_HUNDIDO = Constante.Blanco + "☠" + Constante.reset;
    public static final String Agua = Constante.Azul + "~" + Constante.reset;
    public static final String DISPARO_AGUA = Constante.Rojo+ "X" + Constante.reset;

    public static final int TAMAÑO_TABLERO = 10;
    public static final char[] LETRAS_FILA = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    public static final String HORIZONTAL = "h";
    public static final String VERTICAL = "v";


    public enum Barco{
        Portaaviones,
        Submarino,
        Acorazado,
        Destructor,
        Fragata}




    public enum Reporte{
        Hundido,
        SinDaños,
        Dañado

    }


}