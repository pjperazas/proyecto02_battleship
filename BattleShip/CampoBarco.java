public class CampoBarco implements CampodeJuego {
    private final Barco barco;
    private boolean estaDañado;

    // Crea un espacio en el tablero que contiene una parte de un barco
    public CampoBarco(Barco barco) {
        this.barco = barco;
        this.estaDañado = false;
    }
    // retorna un icono dependiendo del reporte de daños del barco al que pertenece
    public String getIcono() {
        String icono;
        Constante.Reporte estadoBarco = barco.reporteDeDaños();
        switch(estadoBarco) {

            case Dañado:

                if(estaDañado == true){
                    icono = Constante.PARTE_EN_LLAMAS;
                }else{
                    icono = Constante.BARCO_DAÑADO;}
                break;

            case Hundido:
                icono = Constante.BARCO_HUNDIDO;
                break;

            case SinDaños:
                icono = Constante.BARCO_FULLVIDA;
                break;

            default: icono = " ";
                break;
        }
        return icono;
    }

    // Sobre escribe el metodo De recibir disparo de la interfaz de campo de  juego
    // Hace que el barco reciba un punto de daño si no esta hundido
    // y  cambia el boolean estaDañado cuando lo atacan por primera vez
    @Override
    public Constante.Reporte recibirDisparo(){
        Constante.Reporte reporte = barco.reporteDeDaños();
        if (reporte == Constante.Reporte.Hundido){
            barco.recibirDaño();
            return barco.reporteDeDaños();
        }else{
            if (estaDañado==true){ return barco.reporteDeDaños();
            }else{
                barco.recibirDaño();
                estaDañado = true;
                return barco.reporteDeDaños();
            }
        }
    }
}
