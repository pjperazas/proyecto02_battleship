import java.util.Scanner;
    public class BattleShip {
        private Jugador[] jugadores;

        // El contructor añade 2 jugadores
        public BattleShip() {

            this.jugadores = new Jugador[]{
                    new Jugador(1),
                    new Jugador(2)
            };
        }
        // E
        public void terminarTurno(){
            System.out.println("Precione  \"ENTER\" para terminar el turno");
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();

        }
        // Inicia el juego  y repite la funcion de diparar hasta que alguno de los jugadores este sin vida
        public void inicio() {
            int i = 0;
            int j = 1;
            int len = jugadores.length;
            Jugador jugador = null;

            this.jugadores[i].colocarBarcos1();
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

            this.jugadores[j].colocarBarcos2();

            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            System.out.println("Barcos colocados correctamente\nJugador #1 precione  \"ENTER\" para empezar el juego");
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();

            while(jugadores[0].getVidas() > 0 &&
                    jugadores[1].getVidas() > 0) {


                jugadores[i++ % len].dispararA(jugadores[j++ % len]);
                terminarTurno();
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                jugador = (jugadores[0].getVidas() < jugadores[1].getVidas()) ?
                        jugadores[1] :
                        jugadores[0];
            }
            System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            System.out.printf("Jugador %d\n",jugadores[0].getId());
            jugadores[0].getTablero().imprimirTablero();
            System.out.printf("Jugador %d\n",jugadores[1].getId());
            jugadores[1].getTablero().imprimirTablero();
            System.out.println("juego terminado \n");
            System.out.printf("¡El jugador %d, ha gando!",jugador.getId());
        }
    }



