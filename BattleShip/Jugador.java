import java.util.Scanner;
import java.awt.*;
public class Jugador  {

    // Jugador tiene un total de vidas, un id y un tablero y el scanner para inputs

    public int vidas = 21;
    private int id;
    private Tablero tablero;
    private Scanner scanner;

  //Constructor solo ocupa el id
    public Jugador(int id) {
        this.id = id;
        this.tablero = new Tablero();
        this.scanner = new Scanner(System.in);
    }
    // get de id
    public int getId() {
        return id;
    }

    // get de vidas totales
    public int getVidas() {
        return vidas;
    }

    // get del tablero del jugador
    public Tablero getTablero() {
        return tablero;
    }

    // Funcion que coloca el primer set de barcos
    public void colocarBarcos1() {
        System.out.printf("Jugador # %d - Es su turno de colocar sus barcos \n", id);
        tablero.ColocarBarcos1();
    }
    // Funcion que coloca  el segundo set de barcos
    public void colocarBarcos2() {
        System.out.printf("Jugador # %d - Es su turno de colocar sus barcos\n ", id);
        tablero.ColocarBarcos2();
    }
    // Funcion que se le pide unas coordenadas y le envía un disparo al tablero enemigo
    public void dispararA(Jugador oponente) {


        //Aqui se imprime el tablero propio y el del enemigo de manera oculta
        oponente.getTablero().imprimirTableroEnemigo(oponente);
        System.out.println();
        System.out.println();
        tablero.imprimirTablero();
        System.out.printf("%n vidas restantes %d",vidas);
        System.out.println();

        //Aqui se le piden las coordenadas al jugador
        System.out.printf("%n Listo!! jugador %d - Ingrese las coordenadas para su ataque ", id);

        // Se crea un punto con los inputs del jugador

        Point punto = new Point(scanner.nextInt(), scanner.nextInt());
        int y = (int)punto.getX() - 1;
        int x = (int)punto.getY() - 1;

        // Se hace un icono temporal para tener el estado anterior del lugar que se atacó

        String iconoTmp = oponente.getTablero().tablero[x][y].getIcono();

        Constante.Reporte result = oponente.getTablero().tablero[x][y].recibirDisparo();

        // Dependiendo del resultado del ataque se generan distintos resultados

        switch (result) {
            case Dañado:
                if (iconoTmp != Constante.PARTE_EN_LLAMAS){
                    int vida= oponente.vidas;
                    vida--;
                    oponente.vidas = vida; }
                else{ System.out.printf("Esta Parte ya estaba dañada...\n\n");}

                oponente.getTablero().tablero[x][y].recibirDisparo();



                break;


            case Hundido:
                if (iconoTmp != Constante.BARCO_HUNDIDO){
                    int vida1= oponente.vidas;
                    vida1--;
                    oponente.vidas = vida1;}

                else{System.out.printf("Este Barco ya estaba hundido...\n\n");}

                oponente.getTablero().tablero[x][y].recibirDisparo();



                break;


            case SinDaños:
                oponente.getTablero().tablero[x][y].recibirDisparo();
                System.out.printf("Disparo fallido...\n\n");
                break;  }





    }

}