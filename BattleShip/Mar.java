public class Mar implements CampodeJuego {

    // Mar es el campodeJuego neutro cuando recive un ataque su icono cambia a una "X"

    private boolean campoEstaDañado = false;

    @Override
    public String getIcono() {
        return campoEstaDañado ? Constante.DISPARO_AGUA : Constante.Agua;
    }

    @Override
    public Constante.Reporte recibirDisparo() {
        campoEstaDañado = true;
        return Constante.Reporte.SinDaños;
    }
}
