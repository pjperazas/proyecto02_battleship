import java.awt.*;
import java.util.Scanner;


public class Tablero {
    // Tablero tiene un scanner para inputs, una matriz de objetos que se comporten como CampodeJuego
    // y un dos arrays de tipo barco
    private Scanner scanner;
    CampodeJuego[][] tablero;
    private static final Barco[] Barcos1;
    private static final Barco[] Barcos2;

    // Aqui se inicializan los barcos
    static {
        Barcos1 = new Barco[] {
                new Barco(Constante.Barco.Portaaviones, "Portaaviones", 4) {
                },
                new Barco(Constante.Barco.Submarino, "Submario", 3) {
                },
                new Barco(Constante.Barco.Acorazado, "Acorazado", 3) {
                },
                new Barco(Constante.Barco.Submarino, "Submarino", 3) {
                },
                new Barco(Constante.Barco.Destructor, "Destructor", 2) {
                },
                new Barco(Constante.Barco.Destructor, "Destructor", 2) {
                },
                new Barco(Constante.Barco.Destructor, "Destructor", 2) {
                },
                new Barco(Constante.Barco.Fragata, "Fragata", 1) {
                },
                new Barco(Constante.Barco.Fragata, "Fragata", 1) {
                },
        };
    }
    static {
        Barcos2 = new Barco[] {
                new Barco(Constante.Barco.Portaaviones, "Portaaviones", 4) {
                },
                new Barco(Constante.Barco.Submarino, "Submario", 3) {
                },
                new Barco(Constante.Barco.Acorazado, "Acorazado", 3) {
                },
                new Barco(Constante.Barco.Submarino, "Submarino", 3) {
                },
                new Barco(Constante.Barco.Destructor, "Destructor", 2) {
                },
                new Barco(Constante.Barco.Destructor, "Destructor", 2) {
                },
                new Barco(Constante.Barco.Destructor, "Destructor", 2) {
                },
                new Barco(Constante.Barco.Fragata, "Fragata", 1) {
                },
                new Barco(Constante.Barco.Fragata, "Fragata", 1) {
                },


        };
    }
    public Tablero() {
        this.scanner = new Scanner(System.in);
        this.tablero = new CampodeJuego[Constante.TAMAÑO_TABLERO][Constante.TAMAÑO_TABLERO];
        for(int i = 0; i < Constante.TAMAÑO_TABLERO; i++) {
            for(int j = 0; j < Constante.TAMAÑO_TABLERO; j++) {
                tablero[i][j] = new Mar();
            }
        }

    }

    // Esta funcion coloca el primer set de barcos. e imprime el tablero
    public void ColocarBarcos1() {
        for(Barco barco : Barcos1) {

            boolean horizontal = PreguntarDireccionesBarco(barco);

            Point puntoInicio = PreguntarPuntosDeInicio(barco, horizontal);

            LugarValidoDelBarco(barco, puntoInicio, horizontal);

            imprimirTablero();
        }
    }
    // Esta funcion coloca el segundo set de barcos e imprime el tablero
    public void ColocarBarcos2() {
        for(Barco barco : Barcos2) {

            boolean horizontal = PreguntarDireccionesBarco(barco);

            Point puntoInicio = PreguntarPuntosDeInicio(barco, horizontal);

            LugarValidoDelBarco(barco, puntoInicio, horizontal);

            imprimirTablero();
        }
    }


    // Imprime el tablero de principal del jugador usando los iconos de cada campodeJuego
    public void imprimirTablero() {
        System.out.print("Tablero principal\n");
        System.out.print("\t");

        for(int i = 0; i < Constante.TAMAÑO_TABLERO; i++) {
            System.out.print(Constante.LETRAS_FILA[i] + "  ");
        }

        System.out.println();

        for(int i = 0; i < Constante.TAMAÑO_TABLERO; i++) {
            System.out.print((i+1) + "\t");
            for(int j = 0; j < Constante.TAMAÑO_TABLERO; j++) {
                System.out.print(tablero[i][j].getIcono() + "  ");
            }

            System.out.println();
        }
    }
        // Imprime el tablero del enemigo ocultando iconos que no deberia ver el jugador
    public void imprimirTableroEnemigo(Jugador enemigo) {
        System.out.print("Tablero enemigo\n");
        System.out.print("\t");

        for(int i = 0; i < Constante.TAMAÑO_TABLERO; i++) {
            System.out.print(Constante.LETRAS_FILA[i] + "  ");
        }

        System.out.println();

        for(int i = 0; i < Constante.TAMAÑO_TABLERO; i++) {
            System.out.print((i+1) + "\t");
            for(int j = 0; j < Constante.TAMAÑO_TABLERO; j++) {

                if (enemigo.getTablero().tablero[i][j].getIcono() != Constante.Verde + "□" + Constante.reset &&
                        enemigo.getTablero().tablero[i][j].getIcono() != Constante.Rojo + "□" + Constante.reset &&
                        enemigo.getTablero().tablero[i][j].getIcono() != Constante.Negro+ "□" + Constante.reset)
                {

                    System.out.print(enemigo.getTablero().tablero[i][j].getIcono() + "  ");}
                else{System.out.print(Constante.Agua + "  "); }

            }

            System.out.println();
        }
        System.out.printf("vida restante %d",enemigo.getVidas());
    }

    // Esta funcion pregunta la direccion en la que desea colocar un barco
    // con un scaner que elimina los espacios vacios pregunta la direccion: h lo quiere horizontal y v si lo quiere vertical.
    // Repite el scan hasta que el resultado sea h o v
    private boolean PreguntarDireccionesBarco(Barco barco) {
        System.out.printf("Ingrese la direccion de %s: %s (Tamaño  %d): Horizontal(h), Vertical(v) ",barco.getTipo(), barco.getNombre(), barco.getTamaño());
        String direccion;
        do {
            direccion = scanner.nextLine().trim();
        }while (!Constante.HORIZONTAL.equals(direccion) && !Constante.VERTICAL.equals(direccion));

        return Constante.HORIZONTAL.equals(direccion);
    }
    // Esta funcion pregunta el punto de incio de la generacion del barco
    // Solicita al usuario un input de x y y y los ingresa en un objeto de tipo Point
    // Si el input no fue un lugar valido, vuelve a pedir el punto (x,y)

    private Point PreguntarPuntosDeInicio(Barco barco, boolean horizontal) {
        Point iniciaEn;
        do {
            System.out.printf("Ingrese la posicion de %s: %s (Tamaño  %d): ",barco.getTipo(), barco.getNombre(), barco.getTamaño());
            iniciaEn = new Point(scanner.nextInt(), scanner.nextInt());
        } while(!LugarValidoInicio(iniciaEn, barco.getTamaño(), horizontal));

        return iniciaEn;
    }
    // Verifica que un punto para generar un barco este en el tablero y
    // que no al generar los campoBarco no se sobreescriba sobre otro barco o se salga del tablero.

    private boolean LugarValidoInicio(Point iniciaEn, int largo, boolean horizontal) {
        int xDiff = 0;
        int yDiff = 0;
        if(horizontal) {
            xDiff = 1;
        } else {
            yDiff = 1;
        }

        int x = (int)iniciaEn.getX() - 1;
        int y = (int)iniciaEn.getY() - 1;
        if(!estaDentroTablero(x, y) ||
                (!estaDentroTablero(x + largo,y) && horizontal) ||
                (!estaDentroTablero(x, y + largo) && !horizontal)
        ) {
            return false;
        }


        for(int i = 0; i < largo; i++) {
            if(tablero[(int)iniciaEn.getY() + i *yDiff - 1]
                    [(int)iniciaEn.getX() + i *xDiff - 1].getIcono() != Constante.Agua){
                return false;
            }
        }
        return true;
    }

    // Esta funcion usa un barco y un punto valido de generacion y crea los nuevos CampoBarco
    private void LugarValidoDelBarco(Barco barco, Point puntoInicio, boolean horizontal) {
        int xDiff = 0;
        int yDiff = 0;
        if(horizontal) {
            xDiff = 1;
        } else {
            yDiff = 1;
        }
        for(int i = 0; i < barco.getTamaño() ; i++) {
            tablero[(int)puntoInicio.getY() + i*yDiff - 1]
                    [(int)puntoInicio.getX()+ i*xDiff - 1] = new CampoBarco(barco);
        }
    }

    // Esta funcion verifica que un punto y y x no se salgan del margen de la matriz
    private boolean estaDentroTablero(int x, int y){
        return x <= Constante.TAMAÑO_TABLERO && x >= 0
                && y <= Constante.TAMAÑO_TABLERO && y >= 0;
    }
}